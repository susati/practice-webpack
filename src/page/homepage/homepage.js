import React, { Component } from "react";
import IconImage from "../../component/IconImage/IconImage";

class Homepage extends Component {
  render() {
    return (
      <div className="w-100 d-flex justify-content-center align-items-center">
        <h1>Webpack React Setup.</h1>
        <IconImage />
      </div>
    );
  }
}

export default Homepage;