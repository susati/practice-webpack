import React, { Component } from "react";
import { hot } from "react-hot-loader";
import "./App.css";
import Homepage from "./page/homepage/homepage";

class App extends Component {
  render() {
    return <Homepage />;
  }
}

export default hot(module)(App);
