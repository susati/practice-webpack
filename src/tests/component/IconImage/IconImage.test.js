import React from "react";
import { shallow } from "enzyme";
import IconImage from "../../../component/IconImage/IconImage";

describe("IconImage", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(
      <IconImage />
    );
  });

  it("Should render correctly", () => {
    expect(wrapper).toMatchSnapshot()
  })
});