import React, { Component } from "react";

import imgLogo from "../../asset/image/brand.svg";

class IconImage extends Component {
  renderImg() {
    const img = (
      <img
        style={{ width: "50px", height: "50px", margin: "0 10px" }}
        src={imgLogo}
      />
    );
    return img;
  }
  render() {
    return this.renderImg();
  }
}

export default IconImage;
