# base image
FROM node:alpine

# set working directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app/dist

# add `/usr/src/app/node_modules/.bin` to $PATH
ENV PATH /usr/src/app/node_modules/.bin:$PATH

COPY . .
RUN yarn global add serve
RUN yarn install
RUN yarn build

# start app
CMD ["serve"]


# Expose main port
EXPOSE 5000